namespace WebSocket
{
    public class Settings
    {
        public string Protocol { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Db_name { get; set; }
    }
}