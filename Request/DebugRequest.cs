using System.Text.Json.Serialization;

namespace WebSocket.Request
{
    public class DebugRequest
    {
        public string Action {get; set;}
        public string SubAction {get; set;}
        public object Value {get; set;}
    }
}