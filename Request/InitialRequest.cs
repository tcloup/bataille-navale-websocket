namespace WebSocket.Request
{
    public class InitialRequest
    {
        public string Secret {get; set;}
        public int? RoomId {get; set;}
    }
}