using System.Numerics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebSocket.Request
{
    public class AttackRequest
    {
        public string Action {get; set;}
        private Location _position;
        [JsonConverter(typeof(LocationToStringConverter))]
        public string Position
        {
            get => JsonSerializer.Serialize(_position);
            set
            {
                _position = JsonSerializer.Deserialize<Location>(value);
            }
        }

        public Location getPosition()
        {
            return this._position;
        }
    };
}