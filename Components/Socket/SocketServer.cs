using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using System.Threading.Tasks;
using WebSocket.Components.Context;
using WebSocket.Entity;

namespace WebSocket.Components
{
    public class SocketServer
    {
        private int port;
        private List<SocketConnection> connections = new List<SocketConnection>();
        static IPAddress addr = IPAddress.Parse("0.0.0.0");
        private IPEndPoint endpoint;
        private Socket server;
        private EntityContext db;

        public SocketServer(int port, EntityContext db)
        {
            this.port = port;
            this.db = db;
        }

        public async Task StartAsync()
        {
            server = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            while(true)
            {
                endpoint = new IPEndPoint(addr, port);
                try
                {
                    server.Bind(endpoint);
                    server.Listen();
                    break;
                }
                catch(Exception E)
                {
                    Console.WriteLine($"Socket: {E.ToString()}");
                    Console.WriteLine("Socket: Listening failed, retrying in 5 seconds...");
                    await Task.Delay(5000);

                    port = GetPort();
                }
            }

            Console.WriteLine($"Socket: Now listening on port {port}");

            _ = Task.Run(async () => 
            {
                while(true)
                {
                    Socket client = await server.AcceptAsync();
                    Console.WriteLine("Socket: A Client has connected.");
                    connections.Add(new SocketConnection(client, this, this.db));
                    _ = Task.Run(() => connections[connections.Count - 1].ProcessClientAsync());
                }
            });
        }

        public async Task SendToRoom(int roomId, string message)
        {
            IEnumerable<SocketConnection> ConnectionsEnumerable = from connection in connections
                                                                  where connection.Room.Id == roomId
                                                                  select connection;

            foreach(SocketConnection connection in ConnectionsEnumerable)
            {
                await Task.Run(() => connection.Send(message));
            }
        }

        public async Task SendToOpponent(int roomId, Player sender, string message)
        {
            SocketConnection connection = connections.FirstOrDefault(c => c.Room.Id == sender.RoomId && c.Player.Id != sender.Id);

            await Task.Run(() => connection.Send(message));
        }

        public async Task SendDataAsync(int userId, string dataIdentifier, object data)
        {
            IEnumerable<SocketConnection> ConnectionsEnumerable = from connection in connections
                                                                  where connection.Owner.Id == userId
                                                                  select connection;
                                                                  
            foreach (SocketConnection connection in ConnectionsEnumerable)
            {
                string responseString = data.ToString();
                await Task.Run(() => connection.Send("{\""+ dataIdentifier + "\":" + responseString + "}"));     
            }
        }

        public int GetPort()
        {
            TcpListener tcpserver = new TcpListener(IPAddress.Any, 0);
            tcpserver.Start();
            int port = ((IPEndPoint)tcpserver.LocalEndpoint).Port;
            tcpserver.Stop();
            return port;
        }

        public void DisposeConnection(SocketConnection connection)
        {
            connection.CloseConnection();
            connections.Remove(connection);
        }

        public void Dispose()
        {
            for (int i = 0; i != connections.Count; i++)
            {
                connections[i].CloseConnection();
                connections.RemoveAt(i);
            }

            server.Disconnect(false);
            server.Dispose();
        }
    }
}
