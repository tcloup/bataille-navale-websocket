using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Org.BouncyCastle.Utilities;
using WebSocket.Components.Context;
using WebSocket.Entity;
using WebSocket.Request;
using WebSocket.Response;

namespace WebSocket.Components
{
    public class SocketConnection
    {
        public Socket connection;
        private SocketServer socketServer;
        private EntityContext db;
        private DateTime timeSinceLastSuccessfulPing = DateTime.Now;

        public User Owner {get; set;}
        public Player Player {get; set;}
        public Room Room {get; set;}

        public SocketConnection(Socket connection, SocketServer socketServer, EntityContext db)
        {
            this.connection = connection;
            this.socketServer = socketServer;
            this.db = db;
        }

        public async Task ProcessClientAsync()
        {
            await UpgradeConnectionAsync();
            _ = Task.Run(async () => 
            {
                while(true)
                {
                    if (this.Player != null)
                    {
                        new Thread(Ping).Start();

                        if ((DateTime.Now - timeSinceLastSuccessfulPing).TotalSeconds > 30)
                        {
                            if (Room.State != 0 || Room.State != 3)
                            {
                                Console.WriteLine("30 Seconds passed without a successful ping, user probably disconnected");

                                WinningResponse response = Room.GiveWinToOpponent(Player, "Opponent Disconnected");

                                await this.socketServer.SendToRoom(this.Room.Id, JsonSerializer.Serialize(response));

                                await db.SaveChangesAsync();
                            }

                            this.DisposeConnection("Socket: A Client has disconnected, disposing...");

                            break;
                        }
                    }
                    await Task.Delay(1000);
                }
            });
            await ProcessInitialRequestAsync();
        }

        private async Task UpgradeConnectionAsync()
        {   
            byte[] requestdata;
            try
            {
                requestdata = await ReceiveRequestAsync();
            }
            catch(Exception E)
            {
                this.DisposeConnection($"Socket: {E.ToString()}\nSocket: Failed to receive data, closing connection...");
                return;
            }
            string request = BytesToString(requestdata);
            Byte[] response = Encoding.UTF8.GetBytes( "HTTP/1.1 101 Switching Protocols" + Environment.NewLine
                                                    + "Connection: Upgrade" + Environment.NewLine
                                                    + "Upgrade: websocket" + Environment.NewLine
                                                    + "Sec-WebSocket-Accept: " + Convert.ToBase64String(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(new Regex("Sec-WebSocket-Key: (.*)").Match(request).Groups[1].Value.Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"))) + Environment.NewLine+ Environment.NewLine);
            try
            {
                connection.Send(response);
            }
            catch(Exception E)
            {
                this.DisposeConnection($"Socket: {E.ToString()}\nSocket: Failed to send connection upgrade to websocket, closing connection...");
                return;
            }
        }

        private async Task ProcessInitialRequestAsync()
        {
            byte[] requestbytes;
            try
            {
                requestbytes = await ReceiveRequestAsync();
            }
            catch(Exception E)
            {
                this.DisposeConnection($"Socket: {E.ToString()}\n Socket: Failed to receive data, closing connection...");
                return;
            }

            string requestString = DecodeEncodedString(requestbytes);
            Console.WriteLine($"Socket: {requestString}");
            
            InitialRequest request = null;

            try
            {
                request = JsonSerializer.Deserialize<InitialRequest>(requestString);
            }
            catch(Exception e)
            {
                this.DisposeConnection($"{e}\nSocket: Bad request, closing connection...");
                return;
            }

            // Either some kind of serialization issue or nothing serializable?
            if (request.Secret == null || request.RoomId == null)
                this.DisposeConnection("Socket: Bad request, closing connection...");

            var Rooms = db.Rooms.ToList();
            this.Room = db.Rooms.FirstOrDefault(r => r.Id == request.RoomId);

            // Room isn't null and Game state != finished
            if (this.Room == null)
            {
                this.DisposeConnection("Room wasn't found in Database, closing connection...");
                return;
            }
            else if (Room.State == 3)
            {
                this.DisposeConnection("Game has already ended");
                return;
            }

            
            // secret is a random string generated by symfony on login on the website
            if (!string.IsNullOrWhiteSpace(request.Secret) && request.RoomId != null)
            {
                var secret = request.Secret;
                
                this.Owner = db.Users.First(u => u.Secret == secret);
                if (this.Owner == null)
                {
                    this.DisposeConnection("User wasn't found in Database, closing connection...");
                    return;
                }

                this.Player = this.Room.Players.FirstOrDefault(p => p.UserId == Owner.Id);
                if (this.Player == null)
                {
                    if (Room.Slots == 0)
                    {
                        this.DisposeConnection("Player wasn't found in Room because he wasn't part of it, closing connection...");
                    }
                    else
                    {
                        Player player = new Player();
                        player.SetBoats(JsonSerializer.Deserialize<List<Boat>>(db.Gamemodes.Find(1).Boats));
                        player.Room = Room;
                        player.RoomId = Room.Id;
                        player.SetGrid(Newtonsoft.Json.JsonConvert.DeserializeObject<List<List<Cell>>>(db.Grids.Find(1).Cells));
                        player.User = Owner;
                        player.UserId = Owner.Id;

                        db.Players.Add(player);

                        this.Player = player;
                        Room.Slots--;

                        await db.SaveChangesAsync();

                        Console.WriteLine("A new player was added to the game");
                    }
                }
                    
                    

                Console.WriteLine($"Connection validated, handling further request from {Owner.Username} representing Player {Player.Id} in Room {Room.Id}");

                _ = Task.Run(HandleFurtherRequestsAsync);
                return;
            }
        }

        /// <summary>
        /// A method used to process WebSocket Messages
        /// </summary>
        public async Task HandleFurtherRequestsAsync()
        {
            while(true)
            {
                byte[] requestbytes;
                try
                {
                    requestbytes = await ReceiveRequestAsync();
                }
                catch(Exception E)
                {
                    this.DisposeConnection($"Socket: {E.ToString()}\nSocket: Failed to receive data, closing connection...");
                    return;
                }

                string requestString = DecodeEncodedString(requestbytes);

                if (requestString == "pong")
                {
                    timeSinceLastSuccessfulPing = DateTime.Now;
                    continue;
                }

                Dictionary<string, object> request;

                Console.WriteLine($"Socket: {requestString}");

                if (requestString != null)
                    request = JsonSerializer.Deserialize<Dictionary<string, object>>(requestString);
                else
                    continue;

                if (request.Keys.Contains("Action") && !string.IsNullOrEmpty(request["Action"].ToString()))
                    await ProcessRequest(requestString);

                
            }
        }

        public async Task ProcessRequest(string rawRequest)
        {
            var request = JsonSerializer.Deserialize<Dictionary<string, object>>(rawRequest);

            switch(request["Action"].ToString())
            {
                case "start":
                    if (this.Room.Slots == 0 && this.Room.State == 0)
                    {
                        await this.socketServer.SendToRoom(this.Room.Id, "{\"State\": 1}");
                        Room.State = 1;
                        await db.SaveChangesAsync();
                    }
                    return;

                case "debug":
                    if (!Owner.Roles.Contains("ROLE_ADMIN"))
                        return;

                    Console.WriteLine("Socket: Debug request received");
                    DebugRequest debugRequest;

                    try
                    {
                        debugRequest = JsonSerializer.Deserialize<DebugRequest>(rawRequest);
                    }
                    catch(JsonException)
                    {
                        Console.WriteLine("Socket: Unable to deserialize request");
                        return;
                    }

                    if (debugRequest.SubAction == "state")
                    {
                        int state;
                        ((JsonElement)(debugRequest.Value)).TryGetInt32(out state);
                        Room.State = state;
                        await db.SaveChangesAsync();
                        await socketServer.SendToRoom(Room.Id, $"{{\"State\": {state}}}");
                        return;
                    }

                    break;

                case "place":
                    if (Room.State != 1)
                        return;

                    PlacementRequest placementRequest;

                    try
                    {
                        placementRequest = JsonSerializer.Deserialize<PlacementRequest>(rawRequest);
                    }
                    catch(JsonException)
                    {
                        Console.WriteLine("Socket: Unable to deserialize request");
                        return;
                    }
                    
                    if (placementRequest.Id != null && placementRequest.getPosition() != null)
                    {
                        bool isValid = Player.PlaceBoat(placementRequest);

                        await db.SaveChangesAsync();

                        if (isValid)
                        {
                            PlacementResponse placementResponse = new();
                            placementResponse.Result = "Success";
                            placementResponse.Boat = (int)placementRequest.Id;
                            placementResponse.SetPosition(placementRequest.getPosition());
                            placementResponse.SetSize(Player.GetBoats()[placementResponse.Boat].GetSize());

                            Send(JsonSerializer.Serialize(placementResponse));
                        }
                        else
                        {   
                            ErrorResponse ErrorResponse = new();
                            ErrorResponse.Result = "Error";
                            ErrorResponse.Message = $"{Player.GetBoats()[(int)placementRequest.Id].Name ?? $"ID {placementRequest.Id}"} cannot be placed at ({placementRequest.getPosition()})";

                            Send(JsonSerializer.Serialize(ErrorResponse));
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Socket: Request from {this.Owner.Username} ({this.Owner.Id}) was incomplete");
                    }

                    break;
                
                case "attack":
                    if (Room.State != 2)
                        return;

                    // It's the opponent's turn
                    if (Player.Id != Room.Players[Room.Turn].Id)
                        return;

                    AttackRequest attackRequest;

                    try
                    {
                        attackRequest = JsonSerializer.Deserialize<AttackRequest>(rawRequest);
                    }
                    catch(JsonException)
                    {
                        Console.WriteLine("Socket: Unable to deserialize request");
                        return;
                    }

                    bool? result = Player.AttackTile(attackRequest);

                    if (result == null)
                    {
                        ErrorResponse errorResponse = new();
                        errorResponse.Result = "Error";
                        errorResponse.Message = "Cell Already Hit";
                        Send(JsonSerializer.Serialize(errorResponse));
                        return;
                    } 
                    else if(result == true)
                    {
                        AttackResponse attackResponseHit = new();
                        attackResponseHit.Result = "hit";
                        attackResponseHit.SetPosition(attackRequest.getPosition());
                        Send(JsonSerializer.Serialize(attackResponseHit));
                    }
                    else
                    {
                        AttackResponse attackResponseMissed = new();
                        attackResponseMissed.Result = "miss";
                        attackResponseMissed.SetPosition(attackRequest.getPosition());
                        Send(JsonSerializer.Serialize(attackResponseMissed));
                    }

                    _ = Task.Run(() => SendToOpponent("NextTurn"));

                    break;

                default:
                    ErrorResponse response = new();
                    response.Result = "Error";
                    response.Message = "Invalid Action";
                    Send(JsonSerializer.Serialize(response));
                    return;
            }

            if (Room.State == 1)
            {
                int placed = 0;

                foreach(Player player in Room.Players)
                {
                    foreach(Boat boat in player.GetBoats())
                    {
                        if (boat.GetPosition().X != -1 && boat.GetPosition().Y != -1)
                        {
                            placed++;
                        }
                    }
                }

                Console.WriteLine($"{placed} Boats placed");

                if (placed == 10)
                {
                    await socketServer.SendToRoom(Room.Id, $"{{\"State\": 2}}");
                    Room.State = 2;
                    await db.SaveChangesAsync();

                    if (Room.Players[0].Id == Player.Id)
                    {
                        Send("NextTurn");
                    }
                    else
                    {
                        _ = Task.Run(() => SendToOpponent("NextTurn"));
                    }
                }
            }
            
            if (Room.State == 2)
            {
                int boatDown = 0;
                
                foreach(Boat boat in Player.GetBoats())
                {
                    if (boat.Health == 0)
                    {
                        boatDown++;
                    }
                }

                Console.WriteLine($"{boatDown} Boats destroyed so far");

                if (boatDown == Player.GetBoats().Count)
                {
                    Console.WriteLine($"All of {Owner.Username}'s boats are down!");

                    WinningResponse response = Room.GiveWinToOpponent(Player, $"All of {Owner.Username}'s boats are down!");

                    await this.socketServer.SendToRoom(this.Room.Id, JsonSerializer.Serialize(response));

                    await db.SaveChangesAsync();
                }
            }
        }

        public void Ping()
        {
            Send("ping");
        }

        public void Send(string data)
        {
            try
            {
                connection.Send(EncodeDecodedString(data));
            }
            catch(SocketException) 
            {
                Console.WriteLine("Couldn't send message");
            }
            catch(ObjectDisposedException)
            {
                Console.WriteLine("User has already diconnected");
            }
        }

        public async Task SendToOpponent(string data)
        {
            try
            {
                await socketServer.SendToOpponent(Room.Id, Player, data);
            }
            catch(SocketException) 
            {
                Console.WriteLine("Couldn't send message to opponent");
            }
            catch(ObjectDisposedException)
            {
                Console.WriteLine("Opponent has already diconnected");
            }
        }

        private async Task<byte[]> ReceiveRequestAsync()
        {
            byte[] data = new byte[8192];
            byte[] request = null;
            using (MemoryStream ms = new MemoryStream())
            {
                int byteCount;
                do
                {
                    byteCount = connection.Receive(data);
                    await ms.WriteAsync(data, 0, byteCount);
                }
                while (byteCount == 8192);

                request = ms.ToArray();
            }
            return request;
        }

        public string DecodeEncodedString(byte[] data)
        {
            if (data[0] == 129)
            {
                if (data[1] > 127)
                {
                    int length = data[1] - 128;
                    int offset = 2;    
                    if (length == 126)
                    {
                        length = BitConverter.ToUInt16(data[2..4]);
                        offset = 4;
                    }
                    if (length == 127)
                    {
                        length = BitConverter.ToUInt16(data[2..10]);
                        offset = 10;
                    } 
                    // YEP first copy pasta for the decoding part https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_server#decoding_algorithm adapted to use slices like previously
                    byte[] decodedData = new byte[length];
                    byte[] mask = data[offset..(offset + 4)];
                    offset += 4;
                    for (int i = 0; i < length; ++i)
                    {
                        decodedData[i] = (byte)(data[offset + i] ^ mask[i % 4]);
                    }
                    return BytesToString(decodedData);
                }
                else
                {
                    DisposeConnection($"Format Error: Cause by byte at index 1 : {data[1]} <= 127.");
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public byte[] EncodeDecodedString(string response)
        {
            List<byte> data = new List<byte>();
            data.Add(129);
            if (response.Length < 126)
            {
                data.Add((byte)(response.Length));
            }
            if (response.Length > 125 & response.Length < 65536)
            {
                data.Add(126);
                ushort midlength = Convert.ToUInt16(response.Length);
                data.AddRange(BitConverter.GetBytes(midlength).Reverse());
            }
            if (response.Length > 65535)
            {
                data.Add(127);
                ulong longlength = Convert.ToUInt64(response.Length);
                data.AddRange(BitConverter.GetBytes(longlength).Reverse());
            }
            data.AddRange(Encoding.UTF8.GetBytes(response));
            return data.ToArray();
        }

        public string BytesToString(byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        public void DisposeConnection(string message = null)
        {
            Console.WriteLine(message);
            this.socketServer.DisposeConnection(this);
        }

        public void CloseConnection()
        {
            connection.Disconnect(false);
            connection.Dispose();
        }
    }
}