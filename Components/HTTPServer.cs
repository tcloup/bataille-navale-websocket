using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebSocket.Components
{
    class HTTPServer
    {
        public int port;
        public int socketPort;
        public string[] includedPaths = new string[] {"/", "/socketport"};
        HttpListener listener;
        string path = Directory.GetCurrentDirectory();


        public HTTPServer(int port, int socketPort)
        {
            this.port = port;
            this.socketPort = socketPort;
        }

        /// <summary>
        /// Method used to start the server asynchronously
        /// </summary>
        public async Task StartAsync()
        {
            listener = new HttpListener();
            while(true)
            {
                listener.Prefixes.Add($"http://localhost:{port}/");
                try
                {
                    listener.Start();
                    break;
                }
                catch(Exception E)
                {
                    Console.WriteLine($"HTTP Server: {E.ToString()}");
                    Console.WriteLine("HTTP Server: Listening failed, retrying in 5 seconds with another port...");
                    await Task.Delay(5000);

                    port = GetPort();
                }
            }

            Console.WriteLine($"HTTP Server: Now Running on port {port}");

            _ = Task.Run(async () => 
            {
                while(true)
                {
                    var context = await listener.GetContextAsync();
                    answerHTTPGETRequest(context);
                }
            });
        }

        /// <summary>
        /// Method where every requests are processed
        /// </summary>
        /// <param name="client">The context of a connection, usually obtain using (<see cref="HttpListener.GetContextAsync()"/>). </param>
        private void answerHTTPGETRequest(HttpListenerContext client) 
        {
            var response = client.Response;
            var request = client.Request;

            string relativePath = client.Request.RawUrl;
            string filePath = Path.Combine(path, "." + relativePath);

            if (!isFileRequestValid(relativePath, filePath, response))
            {
                return;
            }

            byte[] contents = null;
            switch(relativePath.ToLower())
            {
                case "/":
                    filePath = Path.Combine(path, "index.html");
                    contents = File.ReadAllBytes(filePath);
                    break;
                case "/socketport":
                    contents = Encoding.UTF8.GetBytes(socketPort.ToString());
                    break;
                default:
                    contents = File.ReadAllBytes(filePath);
                    break;
            }
            response.ContentType = request.ContentType;
            response.ContentEncoding = Encoding.UTF8;
            response.ContentLength64 = contents.LongLength;
            response.Close(contents, true);
        }

        /// <summary>
        /// Check if the specified route is a valid route or an existing file
        /// </summary>
        /// <param name="relativePath">The relative path requested by the client</param>
        /// <param name="filePath">The combinaison of local path and relative path</param>
        /// <param name="response">The response that will be used to enventually close the connection</param>
        /// <returns>A boolean indicating if the request is valid</returns>
        public bool isFileRequestValid(string relativePath, string filePath, HttpListenerResponse response)
        {
            if (!includedPaths.Contains(relativePath.ToLower()) & !File.Exists(filePath))
            {
                response.StatusCode = 404;
                response.Close();
                return false;
            }
            return true;
        }

        /// <summary>
        /// generate a random port.
        /// </summary>
        public int GetPort()
        {
            TcpListener tcpserver = new TcpListener(IPAddress.Any, 0);
            tcpserver.Start();
            int port = ((IPEndPoint)tcpserver.LocalEndpoint).Port;
            tcpserver.Stop();
            return port;
        }

        /// <summary>
        /// Stop the HTTP Server
        /// </summary>
        public void Stop()
        {
            listener.Stop();
            listener.Close();
        }
    }
}