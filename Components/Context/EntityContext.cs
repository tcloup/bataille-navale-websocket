using System;
using Microsoft.EntityFrameworkCore;
using WebSocket.Entity;
using System.IO;
using System.Text.Json;
using System.Linq;

namespace WebSocket.Components.Context
{
    public class EntityContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<Grid> Grids { get; set; }

        public DbSet<Gamemode> Gamemodes {get; set; }

        private Settings Settings { get; }

        public EntityContext()
        {
            var content = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "settings.json"));
            this.Settings = JsonSerializer.Deserialize<Settings>(content);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseMySQL($"server={Settings.Host};user={Settings.Username};database={Settings.Db_name};password={Settings.Password};port={Settings.Port}");
    }
}