﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebSocket.Components;
using WebSocket.Components.Context;
using WebSocket.Entity;

namespace WebSocket
{
    class Program
    {
        static private readonly ManualResetEvent resetEvent = new ManualResetEvent(false);
        static private SocketServer socketServer;
        static private HTTPServer httpServer;
        static private EntityContext db;
        
        static void Main(string[] args)
        {
            Console.WriteLine("Starting compnents");
            Task.Run(() => InitializeAsync());
            resetEvent.WaitOne();
        }

        public static async Task InitializeAsync()
        {
            db = new EntityContext();
            await db.Database.GetDbConnection().OpenAsync();
            db.Users.Include(u => u.plays).ToList();
            db.Rooms.Include(r => r.Players).ToList();

            socketServer = new SocketServer(7272, db);
            await socketServer.StartAsync();
            httpServer = new HTTPServer(27272, 7272);
            await httpServer.StartAsync();
        }

        public void Dispose()
        {
            socketServer.Dispose();
            socketServer = null;
            httpServer.Stop();
            httpServer = null;
        }
    }
}
