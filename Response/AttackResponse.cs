using System.Numerics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebSocket.Response
{
    public class AttackResponse
    {
        public string Result {get; set;}

        private Location _position;
        [JsonConverter(typeof(LocationToStringConverter))]
        public string Position 
        {
            get => JsonSerializer.Serialize(_position);
            set
            {
                _position = JsonSerializer.Deserialize<Location>(value);
            }
        }

        public Location GetPosition()
        {
            return _position;
        }

        public void SetPosition(Location position)
        {
            _position = position;
        }
    }
}