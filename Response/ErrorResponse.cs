namespace WebSocket.Response
{
    public class ErrorResponse
    {
        public string Result {get; set;}
        public string Message {get; set;}
        public object[] AdditionalData {get; set;} = null;
    }
}