namespace WebSocket.Response
{
    public class WinningResponse
    {
        public int State {get; set;} = 3;
        public string Info {get; set;}
        public string Details {get; set;}
    }
}