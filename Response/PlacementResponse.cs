using System.Collections.Generic;
using System.Numerics;
using System.Text.Json;

namespace WebSocket.Response
{
    public class PlacementResponse
    {
        public string Result {get; set;}

        public int Boat {get; set;}

        private Location _position;
        public string Position 
        {
            get => JsonSerializer.Serialize(_position);
            set
            {
                _position = JsonSerializer.Deserialize<Location>(value);
            }
        }
        
        private List<int> _size;
        
        public string Size
        {
            get => JsonSerializer.Serialize(_size);
            set
            {
                _size = JsonSerializer.Deserialize<List<int>>(value);
            }
        }

        public Location GetPosition()
        {
            return _position;
        }

        public void SetPosition(Location position)
        {
            _position = position;
        }

        public List<int> GetSize()
        {
            return _size;
        }

        public void SetSize(List<int> size)
        {
            _size = size;
        }
    }
}