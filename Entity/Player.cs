using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using Newtonsoft.Json;
using WebSocket.Request;
using WebSocket.Response;

namespace WebSocket.Entity
{
    [Table("Player")]
    public class Player
    {
        public int Id {get; set;}

        [Column("user_id")]
        public int UserId { get; set; }
        public User User {get; set;}

        public string Grid 
        {
            get => JsonConvert.SerializeObject(_grid); 
            set
            {
                _grid = JsonConvert.DeserializeObject<List<List<Cell>>>(value);
            }
        }
        private List<List<Cell>> _grid = new();

        public string Boats
        {
            get => System.Text.Json.JsonSerializer.Serialize(_boats);
            set 
            {
                _boats = System.Text.Json.JsonSerializer.Deserialize<List<Boat>>(value);

            }
        }
        private List<Boat> _boats;

        [Column("room_id")]
        public int RoomId {get; set;}
        public Room Room {get; set;}

        public List<List<Cell>> GetGrid()
        {
            return _grid;
        }

        public void SetGrid(List<List<Cell>> grid)
        {
            _grid = grid;
        }

        public bool PlaceBoat(PlacementRequest request)
        {
            List<Boat> boats = GetBoats();
            Boat boat = boats[(int)request.Id];
            int x = (int)request.getPosition().X;
            int y = (int)request.getPosition().Y;

            List<List<Cell>> grid = GetGrid();

            for (int xi = 0; xi < boat.GetSize()[0]; xi++)
            {
                for (int yi = 0; yi < boat.GetSize()[1]; yi++)
                {
                    /*
                    * if the clicked cell is a near the right or bottom side and the boat would normally be placed outside the grid
                    * (Avoiding an Array out of range Exception)
                    */
                    if ((x + xi) >= grid.Count || (y + yi) >= grid[0].Count)
                        return false;

                    Cell cell = grid[x + xi][y + yi];

                    if (!cell.IsPlayable || cell.IsOccupied)
                        return false;
                }
            }

            for (int xi = 0; xi < boat.GetSize()[0]; xi++)
            {
                for (int yi = 0; yi < boat.GetSize()[1]; yi++)
                {
                    Cell cell = grid[x + xi][y + yi];
                    cell.SetBoat(boat);
                    cell.IsOccupied = true;
                    grid[x + xi][y + yi] = cell;
                    SetGrid(grid);
                }
            }

            boat.SetPosition(request.getPosition());
            boats[(int)request.Id] = boat;
            SetBoats(boats);

            return true;
        }

        public bool? AttackTile(AttackRequest request)
        {
            Player opponent = Room.Players.First(p => p.Id != Id);
            int turn = Room.Turn;
            int x = (int)request.getPosition().X;
            int y = (int)request.getPosition().Y;
            Cell cell = opponent.GetGrid()[x][y];

                if(cell.Hit == false)
                {
                    if (cell.Boat == null || cell.Boat == "null")
                    {
                        Room.PassTurn();
                        return false;
                    }
                    else
                    {
                        Room.PassTurn();


                        cell.Hit = true;
                        Boat boat = cell.GetBoat();
                        boat.Health--;
                        cell.SetBoat(boat);

                        return true;
                    }
                }
                else
                {
                    return null;
                }
        }

        public List<Boat> GetBoats()
        {
            return _boats;
        }

        public void SetBoats(List<Boat> boats)
        {
            _boats = boats;
        }
    }
}