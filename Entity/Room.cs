using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json;
using WebSocket.Response;

namespace WebSocket.Entity
{
    [Table("Room")]
    public class Room
    {
        public int Id {get; set;}

        public List<Player> Players {get; set;} = new List<Player>();

        /*
        * 0 = Game hasn't started
        * 1 = Placement Period
        * 2 = Attack period
        * 3 = Game finished
        */
        public int State {get; set;}

        public int Slots {get; set;}

        public int Turn {get; set;}

        public void PassTurn()
        {
            Turn = Turn != (Players.Count - 1) ? Turn + 1 : 0;
        }

        public WinningResponse GiveWinToOpponent(Player looser, string details)
        {
            WinningResponse response = new();
            Player opponent = Players.FirstOrDefault(p => p.Id != looser.Id);

            response.Info = $"{opponent.User.Username} Won the Game!";
            response.Details = $"All of {looser.User.Username}'s Boats are down!";

            GivePointsToWinner(opponent, looser);

            return response;
        }

        public void GivePointsToWinner(Player winner, Player looser)
        {
            int pointsLost = (int)Math.Round(0.05 * looser.User.Points, MidpointRounding.AwayFromZero);
            looser.User.Points -= pointsLost;
            winner.User.Points += pointsLost;
        }
    }
}