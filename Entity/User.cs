using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace WebSocket.Entity
{
    [Table("User")]
    public class User
    {
        public int Id {get; set;}

        public string Email {get; set;}

        public string[] _roles;
        public string Roles
        {
            get => JsonSerializer.Serialize(_roles); 
            set
            {
                _roles = JsonSerializer.Deserialize<string[]>(value);
            }
        }

        public string Password {get; set;}

        public List<Player> plays {get; set;}

        public string Username {get; set;}

        public int Points {get; set;} = 1000;

        public string Secret {get; set;}

        public string[] GetRoles()
        {
            return _roles;
        }

        public void setRoles(string[] roles)
        {
            _roles = roles;
        }
    }
}