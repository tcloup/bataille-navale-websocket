using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace WebSocket.Entity
{
    [Table("Grid")]
    public class Grid
    {
        public int Id {get; set;}

        public string Cells
        {
            get => JsonConvert.SerializeObject(_cells); 
            set
            {
                _cells = JsonConvert.DeserializeObject<List<List<Cell>>>(value);
            }
        }
        private List<List<Cell>> _cells = new();

        public string background {get; set;}

        public List<List<Cell>> GetCells()
        {
            return _cells;
        }
    }
}