using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebSocket.Entity
{
    public class Boat
    {
        [JsonConverter(typeof(LocationToStringConverter))]
        public string Position 
        {
            get => JsonSerializer.Serialize(_position);
            set
            {
                _position = JsonSerializer.Deserialize<Location>(value);
            }
        }
        private Location _position;

        public List<int> Size {get; set;}

        public int Health {get; set;}
        
        public string Name {get; set;}

        public Location GetPosition()
        {
            return _position;
        }

        public void SetPosition(Location position)
        {
            _position = position;
        }

        public List<int> GetSize()
        {
            return Size;
        }
    }
}