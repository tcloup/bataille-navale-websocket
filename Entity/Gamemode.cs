using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace WebSocket.Entity
{
    [Table("Gamemode")]
    public class Gamemode
    {
        public int Id {get; set;}
        public string Name {get; set;}

        public string Boats
        {
            get => JsonSerializer.Serialize(_boats);
            set 
            {
                _boats = JsonSerializer.Deserialize<List<Boat>>(value);

            }
        }
        private List<Boat> _boats;

        public List<Boat> GetBoats()
        {
            return _boats;
        }
    }
}