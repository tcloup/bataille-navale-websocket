using System;
using System.Text.Json;

namespace WebSocket.Entity
{
    public class Cell
    {
        public bool IsPlayable {get; set;}

        public bool IsOccupied {get; set;}

        public bool Hit {get; set;}

        private Boat _boat;

        public string Boat 
        {
            get => JsonSerializer.Serialize(_boat); 
            set 
            {
                if (value == null)
                {
                    _boat = null;
                }
                else
                {
                    _boat = JsonSerializer.Deserialize<Boat>(value);
                }
            }
        }

        public Boat GetBoat()
        {
            return _boat;
        }

        public void SetBoat(Boat boat)
        {
            _boat = boat;
        }
    }
}